# Public
Here are all my public Forth source code.

## Swap routines
* swapiflesser  - swap the two top most numbers if the second to top is less than the top most
* swapifgreater - swap the two top most numbers if the second to top is greater than the top most

## Math routines
* gcd           - Greatest common divisor
* lcd           - Least common denominator
* fac           - Factorial
* ncr           - Combinations
* npr           - Permutations
* fib           - Fibonacci numbers
* fibr          - Recursive Fibonacci numbers

## Boolean routines
* pwr2            - Returns -1 on top of stack if number is a "power of 2" else 0
* !pwr2           - Returns -1 on top of stack if number is not "power of 2" else 0
