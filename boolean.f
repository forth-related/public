: pwr2 ( n -- b ) dup 1- and 0= ;
: !pwr2 ( n -- b ) pwr2 0= ;
: pwr2 ( n -- b ) dup dup 0= if drop 0 else 1- and 0= then ;
