needs swap_routines.f
: gcd ( n1 n2 -- n3 ) swapiflesser begin over over = invert while swapiflesser over over - swapiflesser rot drop repeat drop ;
: lcd ( n1 n2 -- n3 ) over over gcd -rot * swap / ;
: fac ( n1 -- n2 ) 1 swap begin  dup 1 > while swap over * swap 1-  repeat  drop ;
: ncr ( n1 n2 -- n3 ) over swap - fac swap fac swap / ;
: npr ( n1 n2 -- n3 ) swap over ncr swap fac / ;
: fib  ( n1 -- n2 ) dup 2 > if 2 - 1 1 rot begin 1- -rot over + swap rot dup 0 > while repeat 2drop else drop 1 then ;
: fibr ( n1 -- n2 ) dup 1 > if 1- dup 1- recurse swap recurse + then ;
